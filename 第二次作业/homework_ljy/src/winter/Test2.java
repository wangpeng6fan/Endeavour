/**
 * 
 */
package winter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *<p>Title:Test2</p>
 *<p>Description:</p>
 * @author ljy 
 * @data 2020年1月30日
 */
/*2_1
 *contains():判断大的字符串中是否含有这个字符串
 *equals():判断两个字符串内容是否一样
 *startsWith():判断是否是以选定的字符串开头
 *endsWith():判断是否是以选定的字符串结尾
 *equals()
 *String s = new String("新年好");
 *String s1 = new String("你好");
 *String s2 = new String("新年好");
 *这时候用equals 当s.equals(s1)为false 但是当s.equals(s2)时为true 但是s不等于s2
 *indexOf()
 *String s = "Happy new years";
 *s1.indexOf("a"); //值为1
 *s1.indexOf("new" 5); //值为6
 *s1.indexOf("b" 3); //值为-1
 *使用indexOf(String str)就是从开始查找出第一个出现要检索的位置 并返回该位置 没查找到就返回-1
 *还有就说 indexOf(String str ,int startpoint) startpoint是要检索的开始位置
 */
/*2_2
 * 1.
 * String 对象的字符串序列的字符不能被修改，删除，即String对象的实体是不可以再发生变化
 * String s = new String("我喜欢过年")
 * StringBuffer类对象的实体的内存空间可以自动改变大小 可以存放可变的字符序列
 * StringBuffer s = new StringBuffer("我喜欢")
 * s.append("过年");
 * 2.
 * String是字符串常量，一旦创建就不能修改；StringBuffer是字符串可变量，可以修改，但是StringBuffer是线程安全的.
 * 选择方法：
 * 如果很少修改，使用String，毕竟它最简单；
 * 如果经常修改，且是多线程，使用StringBuffer。
 */
public class Test2 {
	public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("a");
        list.add("b");
        list.add("d");
        Set set = new  HashSet(); 
        List newList = new  ArrayList(); 
        for (String s:list) {
           if(set.add(s)){
               newList.add(s);
           }
       }
        System.out.println( "去重后的集合： " + newList); 
     }
}