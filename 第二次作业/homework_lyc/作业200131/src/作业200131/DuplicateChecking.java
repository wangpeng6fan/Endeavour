package 作业200131;

import java.util.ArrayList;
import java.util.List;

public class DuplicateChecking {

	public static void main(String[] args) {
		List list=new ArrayList<>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("a");
		list.add("b");
		list.add("d");
		System.out.println("原数组:"+list);
		for(int i=0;i<list.size()-1;i++){
			for(int j=list.size()-1;j>i;j--){
				if(list.get(i).equals(list.get(j))){
					list.remove(j);
				}
			}
		}
		System.out.println("查重后的数组:"+list);

	}

}
