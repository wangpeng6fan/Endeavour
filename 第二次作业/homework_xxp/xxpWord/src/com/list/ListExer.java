package com.list;

import java.util.ArrayList;
import java.util.List;

public class ListExer {

	public static void main(String[] args) {

		List<String> temp = new ArrayList<String>();
		temp.add("a");
		temp.add("b");
		temp.add("c");
		temp.add("a");
		temp.add("b");
		temp.add("d");

		for (int i = 0; i < temp.size() - 1; i++) {
			for (int j = temp.size() - 1; j > i; j--) {
				if (temp.get(j).equals(temp.get(i))) {
					temp.remove(j);
				}
			}
		}
		System.out.println(temp);

	}
}
