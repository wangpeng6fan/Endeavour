package work_hj;

public class w02_2 {
	
	/*
	 * 
	 * 在java中字符串常量就用String类对象表示，字符串变量就用StringBuffer类对象
	 * 表示
	 * 
	 * 
	 * java中每次创建字符串String类的时候都会在内测空间创建新的内存空间，在修改的时候也会创建
	 * 而StringBuffer类修改的时候不会创建新的内存空间，会直接修改原来的内存数据
	 * 
	 * String适用于少量字符串操作的时候使用
	 * 
	 * 
	 */
	public static void main(String[] args) {
		 String s=new String("abc");
		 System.out.println(s);
		 StringBuffer ss=new StringBuffer("adc").append("bb");
		 System.out.println(ss);
	}

}
