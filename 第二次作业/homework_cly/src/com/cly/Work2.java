package com.cly;

import java.util.ArrayList;
import java.util.List;

public class Work2 {
    //前面在文档里
    //题目02_3
    public static List removeDuplicate(List list){
        for(int i=0;i<list.size()-1;i ++){
            for(int j =list.size()-1;j>i;j--){
                if(list.get(j).equals(list.get(i))){
                    list.remove(j);
                }
            }
        }
        return list;
    }

    public static void main(String[] args) {
        List<String> list=new ArrayList<String>(){
            {
                this.add("a");
                this.add("b");
                this.add("c");
                this.add("a");
                this.add("b");
                this.add("d");
            }
        };
        System.out.println("剔除前："+list);
        removeDuplicate(list);
        System.out.println("剔除后"+list);
    }




}
