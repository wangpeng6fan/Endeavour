package com.secondjob.mwm;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
*@author 蒙伟明
* @data 2020年1月30日
*/
public class Aggregatechecking {

	public static void main(String[] args) {
		List<String> list =new ArrayList<String>();
		//添加元素
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("a");
		list.add("b");
		list.add("d");

//		Test1(list);
		Test2(list);

	}
	private static void Test1(List<String> list){

		//初始集合组
		System.out.println(list);
		//判断重复元素
		 for(int i=0;i<list.size()-1;i++) {
	            for(int j=list.size()-1;j>i;j--) {
	                if (list.get(j).equals(list.get(i))) {
	                    list.remove(j);
	                }
	            }
	        }
		 //去掉重复元素集合
		 System.out.println(list);
	}
	private static void Test2(List<String> list) {    
		//创建一个LinkedHashSet集合         
		LinkedHashSet<String> lhs = new LinkedHashSet<>();         
		//将List集合中所有的元素添加到LinkedHashSet集合         
		lhs.addAll(list);         
		//将list集合中的元素清除         
		list.clear();         
		//将LinkedHashSet集合中的元素添加回List集合中        
		list.addAll(lhs);     
		//去掉重复元素集合
		 System.out.println(list);
	}
	}


