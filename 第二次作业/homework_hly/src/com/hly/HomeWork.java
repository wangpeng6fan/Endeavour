package com.hly;

import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author 罗纳尔多.勇
 *题目02_3
    创建List集合{a,b,c,a,b,d}，把集合中的重复元素去除掉，然后打印该List集合。
 */
public class HomeWork {
	 public static void main(String[] args) {
		 List list = new ArrayList(); 
		 list.add("a"); 
		 list.add("b"); 
		 list.add("c"); 
		 list.add("a"); 
		 list.add("b"); 
		 list.add("d"); 
		 System.out.println(list); 
		 for (int i = 0; i < list.size(); i++) { 
			 for (int j = 0; j < list.size(); j++) { 
			 if(i!=j&&list.get(i)==list.get(j)) { 
			 list.remove(list.get(j)); 
			 } 
			 } 
			 } 
			 System.out.println("双重for循环去重:"+list); 
			 list.add("a"); 
			 list.add("b");
	    }
}
