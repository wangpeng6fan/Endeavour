/**
 * 
 */
package demo2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hqr
 * 2020.1.31
 *
 *创建List集合{a,b,c,a,b,d}，把集合中的重复元素去除掉，然后打印该List集合。
 */
public class ListStudy {
     
	public static void main(String [] args){
		ListStudy.test();
	}
	public static void test(){
		List<String> temp = new ArrayList<String>();
		temp.add("acd");
		temp.add("efg");
		temp.add("hij");
		temp.add("klm");
		temp.add("nop");
		temp.add("q");

		for (int i = 0; i < temp.size() - 1; i++) {
			for (int j = temp.size() - 1; j > i; j--) {
				if (temp.get(j).equals(temp.get(i))) {
					temp.remove(j);
				}
			}
		}
		System.out.println(temp);

	}
}

