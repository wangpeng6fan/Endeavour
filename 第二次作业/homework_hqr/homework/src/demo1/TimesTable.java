/**
 * 
 */
package demo1;

/**
 * 寒假第一次作业
 * 2020.1.15
 * @author hqr
 *
 * 右下形态的10-15的乘法表
 */
public class TimesTable{
	

	public static void main(String[] args){
		
		int k=0;
		for (int i = 15; i >= 10; i--) {
			for (int j = 0; j < k; j++) {
				System.out.print("\t\t");
			}
			for (int j = 10; j <=15-k; j++) {
				System.out.print(j + "×" + i + "=" + (j * i)+"\t");
			}
			k++;
			System.out.println();
		}

	}
	}



	


