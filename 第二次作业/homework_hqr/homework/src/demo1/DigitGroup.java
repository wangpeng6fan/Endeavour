/**
 * 
 */
package demo1;

/**
 * 普通数组方法
 * @author hqr
 *2020.1.16
 *
 */
public class DigitGroup {
	String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
	String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
	static int[] stuScore = { 53, 78, 96, 66, 85 };
	
	//最大值
	public static int getMax(int[] stuScore){
		
    int max = stuScore[0];
    for(int i=1;i < stuScore.length; i++ ){
    	if (max < stuScore[i]) {
			max = stuScore[i];
		}
        
	}
    return max;

    }
	
	//最小值
	public static int getMix(int[] stuScore){
		int min= stuScore[0];
		for(int i = 1;i < stuScore.length; i++) {
			if (min> stuScore[i]) {
				min= stuScore[i];
			}
		}
		return min;

	}
	
	
   public static void main(String [] args){
	   
	  System.out.println(DigitGroup.getMax(stuScore)); 
	  System.out.println(DigitGroup.getMix(stuScore)); 
	   
   }
}

