package com.ln.list;

import java.util.ArrayList;
import java.util.List;

public class ListTest {
    /**
     * 
    *<p>Title: main</p>
    *<p>Description: 去掉集合中重复的元素</p>
    　 * @param args
     */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
      List<String> lists= new ArrayList<String>();
      lists.add("a");
      lists.add("b");
      lists.add("c");
      lists.add("a");
      lists.add("b");
      lists.add("d");
      System.out.println("没去重前"+lists);
      for (int i = 0; i < lists.size(); i++) {
    	  for(int j=0;j<lists.size();j++){
    		  if(i!=j&&lists.get(i)==lists.get(j)){
    			  lists.remove(lists.get(j));
    		  }
    	  }
		
	  }
      System.out.println("去重后"+lists);
	}

}
