package work;

import java.util.Arrays;



public class Test {
	//乘法表
public static void P1(){
	
	int k=0;
		for(int i=15;i>=10;i--) {
			for (int j = 0; j < k; j++) {
				System.out.print("\t\t");
			}
			for(int j=10;j<=15-k;j++) {
				
					System.out.print("\t"+i+"*"+j+"="+i*j);
				}
				k++;
				System.out.println();
			}
		}
//计算和，最大最小值，数组排序，复制
	public static void P5(){
		String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
		String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
		int[] stuScore = { 53, 78, 96, 66, 85 };
		
		int max=stuScore[0];

		int min=stuScore[0];

		for(int i=0;i<stuScore.length-1;i++){

		if(max<stuScore[i]){

		max=stuScore[i];

		}

		if(min>stuScore[i]){

		min=stuScore[i];

		}

		}

		System.out.println("最大值："+max);

		System.out.println("最小值："+min);
		int sum=0;
		for(int i = 0 ; i < stuScore.length ; i++){
			
			
			sum += stuScore[i];
			}
		System.out.println("和："+sum);
		

        
		System.out.println("排序前："+Arrays.toString(stuNo));
		Arrays.sort(stuNo);
		System.out.println("排序后："+Arrays.toString(stuNo));
		System.out.println("排序前："+Arrays.toString(stuName));
		Arrays.sort(stuName);
		System.out.println("排序后："+Arrays.toString(stuName));
		System.out.println("排序前："+Arrays.toString(stuScore));
		Arrays.sort(stuScore);
		System.out.println("排序后："+Arrays.toString(stuScore));
		
		int[] newStuScore = Arrays.copyOf(stuScore, stuScore.length);
		System.out.println("新数组"+Arrays.toString(newStuScore));
		
		String[] newStuNo = (String[]) Arrays.copyOf(stuNo, stuNo.length);
		System.out.println("新数组"+Arrays.toString(newStuNo));
		
		String[] newStuName = (String[]) Arrays.copyOf(stuName, stuName.length);
		System.out.println("新数组"+Arrays.toString(newStuName));
		
//		for (int i = 0; i < stuScore.length - 1; i++) {
//			for (int j = 0; j < stuScore.length - i - 1; j++) {
//				if (stuScore[j] > stuScore[j + 1]) {
//					int score = stuScore[j];
//					stuScore[j] = stuScore[j + 1];
//					stuScore[j + 1] = score;
//
//					String no = stuNo[j];
//					stuNo[j] = stuNo[j + 1];
//					stuNo[j + 1] = no;
//
//					String name = stuName[j];
//					stuName[j] = stuName[j + 1];
//					stuName[j + 1] = name;
//				}
//			}
//			
//	}
		
		

	}
	//冒泡排序
	public static void BubbleSort() {
		String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
		String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
		int[] stuScore = { 53, 78, 96, 66, 85 };
		for(int i = 0; i<stuScore.length; i++){
			
			for (int j = stuScore.length-1; j > i; j--) {
				
				if (stuScore[j] < stuScore[j - 1]) {
					
					int score = stuScore[j - 1];
					stuScore[j - 1] = stuScore[j];
					stuScore[j] = score;
					
					String no = stuNo[j-1];
					stuNo[j-1] = stuNo[j ];
					stuNo[j] = no;
					
					String name = stuName[j-1];
					stuName[j-1] = stuName[j];
					stuName[j] = name;
					
				}
			}
		}
		System.out.println(Arrays.toString(stuScore));
		System.out.println(Arrays.toString(stuName));
		System.out.println(Arrays.toString(stuNo));
	}

public static void main(String[] args) {
//	P1();
	//P5();
	BubbleSort();
}
}
