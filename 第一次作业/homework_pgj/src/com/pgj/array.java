package com.pgj;

import java.util.Arrays;
import java.util.stream.IntStream;
/**
 * 
 @ClassName: array 
* @Description:数组 
* @author: PGJ
* @date: 2020年1月17日 下午11:27:09
 */
public class array {

	static String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
	static String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
	static int[] stuScore = { 53, 78, 96, 66, 85 };

	public static void main(String[] args) {
		test1();
		test2();
		test3();
		test4();
	}

	/*
	 * 计算成绩数组中最大值、最小值、所有值之和
	 */
	private static void test1() {
		IntStream intStream = Arrays.stream(stuScore);
		Arrays.sort(stuScore);
		int Min = stuScore[0];
		int Max = stuScore[stuScore.length - 1];
		int sum = intStream.sum();
		System.out.println("最小值：" + Min);
		System.out.println("最大值：" + Max);
		System.out.println("总和：" + sum);

	}

	/*
	 * 分别对以上三组数据排序，向控制台输出排序前和排序后的内容
	 */
	private static void test2() {
		// 学号
		System.out.println("排序前的学号数组：" + Arrays.toString(stuNo));
		Arrays.sort(stuNo);
		System.out.println("排序后的学号数组：" + Arrays.toString(stuNo));

		// 姓名
		System.out.println("排序前的姓名数组：" + Arrays.toString(stuName));
		Arrays.sort(stuName);
		System.out.println("排序后的姓名数组：" + Arrays.toString(stuName));

		// 成绩
		System.out.println("排序前的成绩数组：" + Arrays.toString(stuScore));
		Arrays.sort(stuScore);
		System.out.println("排序后的成绩数组：" + Arrays.toString(stuScore));

	}

	/*
	 * 排序好的内容复制到另外三个数组
	 */
	private static void test3() {
		// 学号
		Arrays.sort(stuNo);
		String[] arr1 = Arrays.copyOf(stuNo, 5);
		System.out.println(Arrays.toString(arr1));

		// 姓名
		Arrays.sort(stuName);
		String[] arr2 = Arrays.copyOf(stuName, 5);
		System.out.println(Arrays.toString(arr2));
		// 成绩
		Arrays.sort(stuScore);
		int[] arr3 = Arrays.copyOf(stuScore, 5);
		System.out.println(Arrays.toString(arr3));

	}

	/*
	 * 根据成绩排序，要求保持学号、姓名、成绩对应
	 */
	private static void test4() {
		// TODO Auto-generated method stub

	}

}
