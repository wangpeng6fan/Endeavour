package hw1_cmh;

public class TimesTables {
	public static void main(String[] args){
		
		int n=0;
		for (int i = 15; i >= 10; i--) {
			for (int j = 0; j < n; j++) {
				System.out.print("\t\t");
			}
			for (int j = 10; j <=15-n; j++) {
				System.out.print(j + "×" + i + "=" + (j * i)+"\t");
			}
			n++;
			System.out.println();
		}

	}
	}