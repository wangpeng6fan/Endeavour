package com.mwm.firstjob;

public class Calculation {

	public static void main(String[] args) {
		Multiplicationtable();

	}

	private static void Multiplicationtable() {
		// 控制行数
		for (int i = 15; i >= 10; i--) {
			// 控制空格数
			for (int k = 0; k < (15 - i); k++)
				System.out.print("          ");
			// 控制每行个数
			for (int j = 10; j <= i; j++) {
				{
					System.out.print(j + "*" + i + "=" + (i * j) + " ");
				}
			}
			// 一行结束换行
			System.out.println();
		}
	}
}
