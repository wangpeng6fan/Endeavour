package com.mwm.firstjob;

import java.util.Arrays;
import java.util.stream.IntStream;

@SuppressWarnings("unused")
public class TopicTwo {

	public static void main(String[] args) {
		// contrast(stuScore);
		// sort(stuNo, stuName, stuScore);
		// copy(stuNo, stuName, stuScore);
		bubbleStuScore(stuNo, stuName, stuScore);
	}

	// 学号
	static String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
	// 姓名数组
	static String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
	// 成绩数组
	static int[] stuScore = { 53, 78, 96, 66, 85 };

	private static void contrast(int[] stuScore) {
		// 数组升序排列
		Arrays.sort(stuScore);
		// 数据流求和
		// IntStream intStream =Arrays.stream(stuScore);
		// int sum = intStream.sum();
		// 最大值最后一个元素
		int max = stuScore[stuScore.length - 1];
		// 最小值是第一个元素
		int min = stuScore[0];

		// 循环求和
		int sum = 0;
		for (int i = 0; i < stuScore.length; i++) {
			sum = sum + stuScore[i];
		}

		System.out.println("最大值: " + max);
		System.out.println("最小值: " + min);
		System.out.println("总和：" + sum);
	}

	public static void sort(String[] stuNo, String[] stuName, int[] stuScore) {
		// 学号排列
		System.out.println("排序前的学号数组：" + Arrays.toString(stuNo));
		Arrays.sort(stuNo);
		//学号升序排列
		System.out.println("升序排序后的学号数组：" + Arrays.toString(stuNo));
		//学号降序排列
		System.out.print("降序排序后的学号数组：");
		for (int i = stuNo.length - 1; i >= 0; i--) {
			System.out.print(stuNo[i] + ",");
		}
		System.out.println();

		// 姓名排列
		System.out.println("排序前的姓名数组：" + Arrays.toString(stuName));
		Arrays.sort(stuName);
		//姓名升序排列
		System.out.println("升序排序后的姓名数组：" + Arrays.toString(stuName));
		//姓名降序排列
		System.out.print("降序排序后的学号数组：");
		for (int i = stuName.length - 1; i >= 0; i--) {
			System.out.print(stuName[i] + ",");
		}
		System.out.println();

		// 成绩排列
		System.out.println("排序前的成绩数组：" + Arrays.toString(stuScore));
		Arrays.sort(stuScore);
		// 成绩升序排列
		System.out.println("升序排序后的成绩数组：" + Arrays.toString(stuScore));
		// 成绩降序排列
		System.out.print("降序排序后的学号数组：");
		for (int i = stuScore.length - 1; i >= 0; i--) {
			System.out.print(stuScore[i] + ",");
		}
		System.out.println();
	}

	public static void copy(String[] stuNo, String[] stuName, int[] stuScore) {
		//复制升序排列后的数组
		String[] newstuNo = Arrays.copyOf(stuNo, stuNo.length);
		String[] newstuName = Arrays.copyOf(stuName, stuName.length);
		int[] newstuScore = Arrays.copyOf(stuScore, stuScore.length);
		System.out.println("学号的新数组：" + Arrays.toString(newstuNo));
		System.out.println("名字的新数组：" + Arrays.toString(stuName));
		System.out.println("成绩的新数组：" + Arrays.toString(stuScore));
	}

	public static void bubbleStuScore(String[] stuNo, String[] stuName, int[] stuScore) {

		for (int i = 0; i < stuScore.length - 1; i++) {
			for (int j = stuScore.length - 1; j > i; j--) {
				//找最大数，如果前一位比后一位大，则交换位置
				if (stuScore[j] > stuScore[j - 1]) {
					//学号交换位置
					String stuno = stuNo[j];
					stuNo[j] = stuNo[j - 1];
					stuNo[j - 1] = stuno;
					//名字交换位置
					String stusname = stuName[j];
					stuName[j] = stuName[j - 1];
					stuName[j - 1] = stusname;
					//成绩交换位置
					int stuscore = stuScore[j];
					stuScore[j] = stuScore[j - 1];
					stuScore[j - 1] = stuscore;

				}
			}
		}
		System.out.println("按成绩从高到低排列后：");
		for (int i = 0; i < stuScore.length; i++) {
			System.out.println(stuNo[i] + "\t" + stuName[i] + "\t" + stuScore[i]);
		}
	}
}
