package com.hxc;

import java.util.Arrays;

public class Array {

	public static void main(String[] args) {
		test1();

	}

	private static void test1() {
		String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
		String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
		int[] stuScore = { 53, 78, 96, 66, 85 };
		int max= stuScore[0];
		int min= stuScore[0];
		int sum= 0;
		for(int i=0;i<stuScore.length;i++){
			if (max<stuScore[i]) {
				max=stuScore[i];
			}
			if (min>stuScore[i]) {
				min=stuScore[i];
			}
			sum=sum+stuScore[i];
		}
		System.out.println("{最大值:"+max+"\t"+"最小值:"+min+"\t"+"所有值的和:"+sum+"}");
		System.out.print("{学号排序前:");
		for(int i=0;i<stuNo.length;i++){
			System.out.print(stuNo[i]+"\t");
		}
		System.out.print("学号排序后:");
		Arrays.sort(stuNo);//数组排序
		for(int i=0;i<stuNo.length;i++){
			System.out.print(stuNo[i]+"\t");
		}
		System.out.print("}");
		System.out.println();
		
		System.out.print("{姓名排序前:");
		for(int i=0;i<stuName.length;i++){
			System.out.print(stuName[i]+"\t");
		}
		System.out.print("姓名排序后:");
		Arrays.sort(stuName);//数组排序
		for(int i=0;i<stuName.length;i++){
			System.out.print(stuName[i]+"\t");
		}
		System.out.print("}");
		System.out.println();
		
		System.out.print("{成绩排序前:");
		for(int i=0;i<stuScore.length;i++){
			System.out.print(stuScore[i]+"\t");
		}
		Arrays.sort(stuScore);
		System.out.print("成绩排序后:"+Arrays.toString(stuScore)+"}");
		/*for(int i=0;i<stuScore.length-1;i++){//走多少次
			for(int j=1;j<stuScore.length-i;j++){//比较多少次
				int k;
				if (stuScore[j]<stuScore[j-1]){
					k=stuScore[j-1];
					stuScore[j-1]=stuScore[j];
					stuScore[j]=k;	
				}
			}
		}
		for(int i=0;i<stuScore.length;i++){
			System.out.print(stuScore[i]+"\t");
		}*/
		System.out.println();
		String[] stuNO1=Arrays.copyOf(stuNo, stuNo.length);
		String[] stuName1=Arrays.copyOf(stuName, stuName.length);
		int[] stuScore1=Arrays.copyOf(stuScore, stuScore.length);
		System.out.println("复制学号数组:"+Arrays.toString(stuNO1));
		System.out.println("复制名字数组:"+Arrays.toString(stuName1));
		System.out.println("复制成绩数组:"+Arrays.toString(stuScore1));
	}

}
