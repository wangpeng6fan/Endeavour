package com.qyx;

import java.sql.Array;
import java.util.Arrays;

public class title01_2 {
      static String[] stuNo={"2019011535","2019011534","2019011539","2019011538","2019011537"};
      static String[] stuName={"张三","李四","王五","赵六","王九"};
      static int[] stuScore={53,78,96,66,85};

	public static void stuScoremessage(){
		int max=stuScore[0];
		int min=stuScore[0];
		int count=0;
		for(int i=0;i<=stuScore.length-1;i++){
			if (max<stuScore[i]) {
				max=stuScore[i];
			}
			if (min>stuScore[i]) {
				min=stuScore[i];
			}
			 count+=stuScore[i];
		}
		System.out.println("stuScore数组中最大值为:"+max);
		System.out.println("stuScore数组中最小值为:"+min);
		System.out.println("stuScore数组中所有值的和为:"+count);
	}
	public static  void  Sorts(){
		System.out.println("stuNo数组排序前"+Arrays.toString(stuNo));
		Arrays.sort(stuNo);
		System.out.println("stuNo数组排序后"+Arrays.toString(stuNo));
		
		System.out.println("stuNo数组排序前"+Arrays.toString(stuName));
		Arrays.sort(stuName);
		System.out.println("stuNo数组排序后"+Arrays.toString(stuName));
		
		System.out.println("stuNo数组排序前"+Arrays.toString(stuScore));
		Arrays.sort(stuScore);
		System.out.println("stuNo数组排序后"+Arrays.toString(stuScore));
	}
	public static void copy(){
	  Arrays.sort(stuScore);
	  Arrays.sort(stuNo);
	  Arrays.sort(stuName);
	  int[] stuScore1 =Arrays.copyOf(stuScore,5);
	  String[] stuName1=Arrays.copyOf(stuName, 5);
	  String[] stuNo1=Arrays.copyOf(stuName, 5);
	}
    public static void main(String args[]){
    	stuScoremessage();
    	Sorts();
    	copy();
    }
	
}
