package 数组;

import java.util.Arrays;

public class Two {

	public static void main(String[] args) {
		String[] stuNo=new String[]{"2019011535", "2019011534", "2019011539", "2019011538", "2019011537"};
		String[] stuName=new String[]{"张三", "李四", "王五", "赵六", "王九"};
		int[] stuScore=new int[]{53, 78, 96, 66, 85};
		System.out.println();
		int max=0;
		int min=100;
		int sum=0;
		for(int i=0;i<stuScore.length;i++){
			if(stuScore[i]>max){
				max=stuScore[i];
			}
			if(stuScore[i]<min){
				min=stuScore[i];
			}
			sum=sum+stuScore[i];
		}
		String stuNo1=Arrays.toString(stuNo);
		String stuName1=Arrays.toString(stuName);
		String stuScore1=Arrays.toString(stuScore);
		System.out.println("最高成绩是"+max);
		System.out.println("最低成绩是"+min);
		System.out.println("所有成绩和是"+sum+"\n");
		
		System.out.println("排序前学号\n"+stuNo1);
		System.out.println("排序前姓名\n"+stuName1);
		System.out.println("排序前成绩\n"+stuScore1);
		Arrays.sort(stuNo);
		Arrays.sort(stuName);
		Arrays.sort(stuScore);
		System.out.println("排序后学号");
		for(int i=0;i<stuNo.length;i++){
			System.out.print(stuNo[i]+"  ");
		}
		System.out.println("\n"+"排序后姓名");
		for(int i=0;i<stuName.length;i++){
			System.out.print(stuName[i]+"  ");
		}
		System.out.println("\n"+"排序后成绩");
		for(int i=0;i<stuScore.length;i++){
			System.out.print(stuScore[i]+"  ");
		}
		String[] stuNo2=Arrays.copyOf(stuNo, stuNo.length);
		String[] stuName2=Arrays.copyOf(stuName, stuName.length);
		int[] stuScore2=Arrays.copyOf(stuScore, stuScore.length);
		
		System.out.println("\n\n新数组\n学号"+Arrays.toString(stuNo2)+"\n姓名"+Arrays.toString(stuName2)+"\n成绩"+Arrays.toString(stuScore2));
		
	}

}
