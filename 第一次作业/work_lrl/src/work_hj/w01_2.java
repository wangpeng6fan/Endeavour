package work_hj;

import java.util.Arrays;

public class w01_2 {
	public static void main(String[] args) {
		String[] stuNo={"2019011535", "2019011534", "2019011539", "2019011538", "2019011537"};
		String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
		int[] stuScore = { 53, 78, 96, 66, 85 };
		System.out.println("(1)");
		//计算成绩数组中最大值、最小值、所有值之和，并输出到控制台。
		int s=0;
		int[] stuScore1=Arrays.copyOf(stuScore, 5);
		System.out.println(stuScore1[0]);
		System.out.println(stuScore1[4]);
		for(int i=0;i<stuScore.length;i++){
			s=s+stuScore[i];
		}
		System.out.println(s);
		
		System.out.println("(2)");
		//分别对以上三组数据排序，向控制台输出排序前和排序后的内容。		
		String[] stuNo1=Arrays.copyOf(stuNo, 5);
		Arrays.sort(stuNo1);
		System.out.println(Arrays.toString(stuNo));
		System.out.println(Arrays.toString(stuNo1));
		
		String[] stuName1=Arrays.copyOf(stuName, 5);
		Arrays.sort(stuName1);
		System.out.println(Arrays.toString(stuName));
		System.out.println(Arrays.toString(stuName1));
		
		int[] stuScore2=Arrays.copyOf(stuScore, 5);
		Arrays.sort(stuScore2);
		System.out.println(Arrays.toString(stuScore));
		System.out.println(Arrays.toString(stuScore2));
		
		System.out.println("(3)");
		//将（2）中排序好的内容复制到另外三个数组中。
//		Arrays.fill(stuNo1, stuName1);
//		System.out.println(Arrays.toString(stuName1));
		String[] stuNo2=Arrays.copyOf(stuNo1, 5);
		String[] stuNo3=Arrays.copyOf(stuNo1, 5);
		String[] stuNo4=Arrays.copyOf(stuNo1, 5);
		System.out.println(Arrays.toString(stuNo2));
		System.out.println(Arrays.toString(stuNo3));
		System.out.println(Arrays.toString(stuNo4));
		String[] stuName2=Arrays.copyOf(stuName1, 5);
		String[] stuName3=Arrays.copyOf(stuName1, 5);
		String[] stuName4=Arrays.copyOf(stuName1, 5);
		System.out.println(Arrays.toString(stuName2));
		System.out.println(Arrays.toString(stuName3));
		System.out.println(Arrays.toString(stuName4));
		int[] stuScore3=Arrays.copyOf(stuScore2, 5);
		int[] stuScore4=Arrays.copyOf(stuScore2, 5);
		int[] stuScore5=Arrays.copyOf(stuScore2, 5);
		System.out.println(Arrays.toString(stuScore3));
		System.out.println(Arrays.toString(stuScore4));
		System.out.println(Arrays.toString(stuScore5));
		
		System.out.println("(4)");
		//根据成绩排序，要求保持学号、姓名、成绩对应（选作）。
		String[] ww=Arrays.copyOf(stuName, stuName.length);
		int[] vv=Arrays.copyOf(stuScore, stuScore.length);
		System.out.println(Arrays.toString(vv));
		System.out.println(Arrays.toString(ww));
		System.out.println();
		for(int i=0;i<vv.length-1;i++){
			for(int l=0;l<vv.length-1;l++){
				if(vv[l]<vv[l+1]){
					int x=vv[l];
					vv[l]=vv[l+1];
					vv[l+1]=x;
					
					String y=ww[l];
					ww[l]=ww[l+1];
					ww[l+1]=y;
				}
			}
		}
		System.out.println("绩排名如下：");
		System.out.println(Arrays.toString(vv));
		System.out.println("按成绩排名名字如下：");
		System.out.println(Arrays.toString(ww));
		
//		System.out.println(stuNo1[0]);
//		System.out.println(stuNo1[4]);
	}

}
