package zuoye01_2;

import java.util.Arrays;

public class Array {
	public static void main(String[] args) {
		String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
		String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
		int[] stuScore = { 53, 78, 96, 66, 85 };
		
		int sum=0,max=stuScore[0],min=stuScore[0];
		
		for(int i=0;i<stuScore.length;i++){
			
			if(max<stuScore[i]){
				max=stuScore[i];
			}
			if(min>stuScore[i]){
				min=stuScore[i];
			}
			
			sum+=stuScore[i];
		}

		System.out.println("成绩数组的和值为:"+sum);
		System.out.println("成绩数组的最大值为:"+max);
		System.out.println("成绩数组的最小值为:"+min);
		System.out.println("----------------------------");
		
		
		System.out.println("排序前的学号数组为："+Arrays.toString(stuNo));
		Arrays.sort(stuNo);
		System.out.println("排序后的学号数组为："+Arrays.toString(stuNo));
		System.out.println("----------------------------");
		
		System.out.println("排序前的姓名数组为："+Arrays.toString(stuName));
		Arrays.sort(stuName);
		System.out.println("排序后的姓名数组为："+Arrays.toString(stuName));
		System.out.println("----------------------------");
		
		System.out.println("排序前的成绩数组为："+Arrays.toString(stuScore));
		Arrays.sort(stuScore);
		System.out.println("排序后的成绩数组为："+Arrays.toString(stuScore));
		System.out.println("----------------------------");
		
		//复制数组
		String[] stuNo1 = Arrays.copyOf(stuNo, stuNo.length);
		System.out.println("复制学号数组为："+Arrays.toString(stuNo1));
		
		String[] stuName1 = Arrays.copyOf(stuName, stuName.length);
		System.out.println("复制姓名数组为："+Arrays.toString(stuName1));
		
		int[] stuScore1 = Arrays.copyOf(stuScore, stuScore.length);
		System.out.println("复制成绩数组为："+Arrays.toString(stuScore1));
		
	}

}
