package zuoye;

import java.util.Arrays;

public class Zuoye2 {

public static void main(String [] args){
	//test1(stuScore);
	//test2(stuNo, stuName, stuScore);
	test3(stuNo, stuName, stuScore);
	
}


	
		// TODO Auto-generated method stub

		//学号数组：
		static String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
		//姓名数组：
		static String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
		//成绩数组：
		static int[] stuScore = new int[]{ 53, 78, 96, 66, 85};
	
		
		
	public static void test1(int[] stuScore){	
		
		//sort会把原本的数组按照从小到大的顺序排列，没有返回值。
		System.out.println("排序前"+Arrays.toString(stuScore));
		Arrays.sort(stuScore);
		
		System.out.println("排序后"+Arrays.toString(stuScore));
		int firstNum = stuScore[0];//最小值
		int lastNum = stuScore[stuScore.length-1];// 最大值
		
		System.out.println("最小值"+firstNum);
		System.out.println("最大值"+lastNum);
		
		//for求数组中所数之和
		int sum=0;
		
        for(int i=0;i<stuScore.length;i++)
        {
            sum +=stuScore[i];
           
            
        }
        System.out.println("所有值的和"+sum);
	}
	
	//排序
	public static void test2(String[] stuNo,String[] stuName,int[] stuScore){	
     //学号
        System.out.println("排序前"+Arrays.toString(stuNo));
        Arrays.sort(stuNo);
        System.out.println("排序后"+Arrays.toString(stuNo));
        //复制
        String[] stuNo1 = Arrays.copyOf(stuNo, 10);
        System.out.println("复制后输出"+Arrays.toString(stuNo1));
        
        //姓名
        System.out.println("排序前"+Arrays.toString(stuName));
        Arrays.sort(stuName);
        System.out.println("排序后"+Arrays.toString(stuName));
        //复制
        String[] stuName1 = Arrays.copyOf(stuName, 10);
        System.out.println("复制后输出"+Arrays.toString(stuName1));
        
        //成绩
        System.out.println("排序前"+Arrays.toString(stuScore));
		Arrays.sort(stuScore);
		System.out.println("排序后"+Arrays.toString(stuScore));
		  //复制
        int[] stuScore1 = Arrays.copyOf(stuScore, 10);
        System.out.println("复制后输出"+Arrays.toString(stuScore1));
    }

	
		 public static void test3(String[] stuNo,String[] stuName,int[] stuScore){

			 for (int i = 0 ; i< stuScore.length ; i++) {
				    System.out.printf("%s %s %s%n", stuScore[i], stuName[i],stuNo[i]);  
			 }
		 System.out.println("排序后");
		 
		 //冒泡排序
		 //创建一个外层for循环，用于控制循环次数


		 for(int i=0;i<stuScore.length-1;i++){
			 
			 //创建内层循环用于判断把最大的数往后放置，每次找到最大的数值时循环次数会减少一次
			 for(int j=0;j<stuScore.length-1;j++){
				 //If判断，如果当前数比下一个数大
				 if(stuScore[j]>stuScore[j+1]){
				 //开始交换位置，把下一个数赋值给temp,然后把当前数赋值给下一个数，最后在把temp的值给当前数，这样就把2个数值互换了位置。
				 int temp = stuScore[j+1];
				 stuScore[j+1]=stuScore[j];
				 stuScore[j] = temp;
				 
				 String temp1 =stuName[j+1];
				 stuName[j+1]=stuName[j];
				 stuName[j]=temp1;

				 String temp2 =stuNo[j+1];
				 stuNo[j+1]=stuNo[j];
				 stuNo[j]=temp2;
				 }
			 }
		 }
			 for (int i = 0 ; i< stuScore.length ; i++) {
				    System.out.printf("%s %s %s%n", stuScore[i], stuName[i],stuNo[i]);  
				
		 
		 }
	}
}
