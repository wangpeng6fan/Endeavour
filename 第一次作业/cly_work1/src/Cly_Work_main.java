public class Cly_Work_main {
    public static void main(String[] args) {//idea:psvm一键生成main方法
        String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };//学号数组
        String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };//姓名数组
        int[] stuScore = { 53, 78, 96, 66, 85 };//成绩数组
        Work1.NineAndNine();
        Work2.OneAndTwo(stuNo,stuName,stuScore);
        Work2.Three(stuNo,stuName,stuScore);
        Work2.Four(stuNo,stuName,stuScore);
    }
}
