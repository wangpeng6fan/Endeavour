import java.sql.SQLOutput;
import java.util.Arrays;

public class Work2 {
    public static void OneAndTwo(String[] stuNo, String[] stuName, int[] stuScore) {
        System.out.println("Cly_Work_1.2");
//（1）计算成绩数组中最大值、最小值、所有值之和，并输出到控制台。
        int sum=0;
        int[] stuScore1=stuScore;
        for (int i:stuScore1){
            sum+=i;
        }
        System.out.println("（1）\t最大值为:"+stuScore[stuScore1.length-1]+"\t\t最小值为:"+stuScore1[0]+"\t\t所有值之和为:"+sum);

//（2）分别对以上三组数据排序，向控制台输出排序前和排序后的内容。
    //学号排序
        System.out.print("（2）\t学号排序前：");
        for (String no:stuNo){
            System.out.print(no+"\t");
        }
        System.out.print("\n\t\t学号排序后：");
        Stringpx(stuNo);

    //名字排序
        System.out.print("\n\t\t名字排序前：");
        for (String no:stuName){
            System.out.print(no+"\t");
        }
        System.out.print("\n\t\t名字排序后：");
        Stringpx(stuName);
    //成绩排序
        System.out.print("\n\t\t成绩排序前：");
        for (int i:stuScore){
            System.out.print(i+"\t");
        }
        Arrays.sort(stuScore1);
        System.out.print("\n\t\t成绩排序后：");
        for (int i:stuScore){
            System.out.print(i+"\t");
        }
    }
//（3）将（2）中排序好的内容复制到另外三个数组中。
    public static void Three(String[] stuNo, String[] stuName, int[] stuScore){
        System.out.println("\n（3）");
        Stringpx(stuNo);
        String[] stuNo1=stuNo;

        Stringpx(stuName);
        String[] stuName1=stuName;

        Arrays.sort(stuScore);
        int[] stuScire1=stuScore;
        for (int i:stuScire1){
            System.out.print(i+"\t");
        }
    }
//（4）根据成绩排序，要求保持学号、姓名、成绩对应（选作）。
    public static void Four(String[] stuNo, String[] stuName, int[] stuScore){
        System.out.println("\n（4）");
        System.out.println("根据成绩排序前：\n姓名\t学号\t\t成绩");
        for (int i = 0; i < stuScore.length; i++) {//foreach引用的是数组元素，所以这里不能用增强型for循环
            System.out.println( stuName[i] + "\t" +stuNo[i] + "\t" + stuScore[i]);
        }
        for (int i:stuScore) {
            for (int j = 0; j < stuScore.length - i - 1; j++) {
                if (stuScore[j] > stuScore[j + 1]) {
                    mppx(stuNo,i,j);
                    mppx(stuName,i,j);
                    mppx(stuScore,i,j);
                }
            }
        }
        System.out.println("根据成绩排序后:\n姓名\t学号\t\t成绩");
        for (int i = 0; i < stuScore.length; i++) {
            System.out.println( stuName[i] + "\t" +stuNo[i] + "\t" + stuScore[i]);
        }
    }
//比较ASCII码实现字符串排序
    public static String[] arraySort(String[] input){
        for (int i=0;i<input.length-1;i++){
            for (int j=0;j<input.length-i-1;j++) {
                if(input[j].compareTo(input[j+1])>0){
                    String temp=input[j];
                    input[j]=input[j+1];
                    input[j+1]=temp;
                }
            }
        }
        return input;
    }
//（3）减少重复代码
    public static void Stringpx(String[] i){
        String[] output=arraySort(i);
        for (String no:output){
            System.out.print(no+"\t");
        }
        System.out.println();
    }
//（4）的冒泡排序重载
    public static void mppx(String[] stu,int i,int j){
        String x = stu[j];
        stu[j] = stu[j + 1];
        stu[j + 1] = x;
    }
    public static void mppx(int[] stu,int i,int j){
        int x = stu[j];
        stu[j] = stu[j + 1];
        stu[j + 1] = x;
    }
}
