package com.shuzu;

import java.util.Arrays;
import java.util.Collections;

public class Test2 {

	static String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
	static String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
	static int[] stuScore = { 53, 78, 96, 66, 85 };

	// （1）计算成绩数组中最大值、最小值、所有值之和，并输出到控制台。
	// 输出数组最大和最小值
	public static void tests1(int[] stuScore) {
		int max = stuScore[0];
		int min = stuScore[0];
		for (int i = 0; i < stuScore.length - 1; i++) {
			if (max < stuScore[i]) {
				max = stuScore[i];
			}
			if (min > stuScore[i]) {
				min = stuScore[i];
			}
		}
		System.out.println("最大值：" + max);
		System.out.println("最小值：" + min);

		// 输出数组总和
		int sum = 0;
		for (int i = 0; i < stuScore.length - 1; i++) {
			sum += stuScore[i];
		}
		System.out.println("数组总和:" + sum);
	}

	// （2）分别对以上三组数据排序，向控制台输出排序前和排序后的内容。
	public static void tests2(String[] stuNo, String[] stuName, int[] stuScore) {
		System.out.println("排序前" + Arrays.toString(stuScore));
		Arrays.sort(stuScore);
		System.out.println("排序后" + Arrays.toString(stuScore));

		System.out.println("排序前" + Arrays.toString(stuNo));
		Arrays.sort(stuNo);
		System.out.println("排序后" + Arrays.toString(stuNo));

		System.out.println("排序前" + Arrays.toString(stuName));
		Arrays.sort(stuName);
		System.out.println("排序后" + Arrays.toString(stuName));
	}

	// （3）将（2）中排序好的内容复制到另外三个数组中。
	public static void tests3(String[] stuNo, String[] stuName, int[] stuScore) {
		int[] newStuScore = Arrays.copyOf(stuScore, 5);
		System.out.println(Arrays.toString(newStuScore));

		String[] newStuNo = Arrays.copyOf(stuNo, stuNo.length);
		System.out.println(Arrays.toString(newStuNo));

		String[] newStuName = Arrays.copyOf(stuName, stuName.length);
		System.out.println(Arrays.toString(newStuName));
	}

	//(4)根据成绩排序，要求保持学号、姓名、成绩对应（选作）。
	public static void tests4(String[] stuNo, String[] stuName, int[] stuScore) {
		System.out.println("排序前");
		for (int i = 0; i < stuScore.length; i++) {
			System.out.println( stuName[i] + "\t" +stuNo[i] + "\t" + stuScore[i]);
		}
		for (int i = 0; i < stuScore.length - 1; i++) {

			for (int j = 0; j < stuScore.length - i - 1; j++) {
				if (stuScore[j] > stuScore[j + 1]) {
					int num = stuScore[j];
					stuScore[j] = stuScore[j + 1];
					stuScore[j + 1] = num;

					String no = stuNo[j];
					stuNo[j] = stuNo[j + 1];
					stuNo[j + 1] = no;

					String name = stuName[j];
					stuName[j] = stuName[j + 1];
					stuName[j + 1] = name;
				}
			}
		}
		System.out.println("排序后");
		for (int i = 0; i < stuScore.length; i++) {
			System.out.println( stuName[i] + "\t" +stuNo[i] + "\t" + stuScore[i]);
		}

	}

	// for(int i= 0;i<stuScore.length;i++){
	// System.out.println("姓名："+
	// stuName[i]+"\t"+"学号："+stuNo[i]+"\t"+"成绩："+stuScore[i]+"\t");

	public static void main(String[] args) {
//		Test2.tests1(stuScore);
//		Test2.tests2(stuNo, stuName, stuScore);
		Test2.tests3(stuNo, stuName, stuScore);
//		Test2.tests4(stuNo, stuName, stuScore);
	}
}
