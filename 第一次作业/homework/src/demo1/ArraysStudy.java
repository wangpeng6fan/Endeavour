/**
 * 
 */
package demo1;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Arrays
 * @author hqr 
 * 2020.1.16
 *
 */
public class ArraysStudy {
	
	static String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
	static 	String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
    static int[] stuScore = { 53, 78, 96, 66, 85 };
 
  /**
   * 1.求成绩最大最小值以及总和
   * Arrays.sort();方法已经将(成绩数组)stuScore按从小到大顺序排序好，只需将第一和最后一个数输出即可
   * 
   */
    public static void test1(int[] stuScore){	
    	IntStream intStream =Arrays.stream(stuScore);
    	Arrays.sort(stuScore);
    	int Min = stuScore[0];
    	int Max = stuScore[stuScore.length - 1];
        int sum = intStream.sum();
        
       
		System.out.println("最小值："+Min);
		System.out.println("最大值："+Max);
		System.out.println("总和："+sum);
		 System.out.println("—————————————————————————————————");
    }
    
    
    
    /**
     * 2.分别对以上三组数据排序，向控制台输出排序前和排序后的内容
     * toString();和sort();的运用
     * 
     */
  public static void test2( String[] stuNo,String[] stuName,int[] stuScore){
   //学号
	System.out.println("排序前的学号数组："+Arrays.toString(stuNo));
	Arrays.sort(stuNo);
	System.out.println("排序后的学号数组："+Arrays.toString(stuNo));
	
	//姓名
	System.out.println("排序前的姓名数组："+Arrays.toString(stuName));
	Arrays.sort(stuName);
	System.out.println("排序后的姓名数组："+Arrays.toString(stuName));
	
	   //成绩
		System.out.println("排序前的成绩数组："+Arrays.toString(stuScore));
		Arrays.sort(stuScore);
		System.out.println("排序后的成绩数组："+Arrays.toString(stuScore));
		}
  
  
   /**
   * 3.将（2）中排序好的内容复制到另外三个数组中
   * copyOf();的运用
   */
  public static void test3( String[] stuNo,String[] stuName,int[] stuScore ){
	  
	  //学号
	  Arrays.sort(stuNo);
	  String[] arr1 = Arrays.copyOf(stuNo, 5);
	  System.out.println(Arrays.toString(arr1));
	  
	  //姓名
	  Arrays.sort(stuName);
	 String[] arr2 = Arrays.copyOf(stuName, 5);
	 System.out.println(Arrays.toString(arr2));
	  //成绩
	  Arrays.sort(stuScore);
	  int[] arr3 = Arrays.copyOf(stuScore, 5);
	  System.out.println(Arrays.toString(arr3));
  }

  /**
   * 
  * 4.根据成绩排序，要求保持学号、姓名、成绩对应（选作）
  * 
  */
  
   public static void test4(String[] stuNo,String[] stuName,int[] stuScore){
	   System.out.println("排序前");
	   for(int i = 0; i < stuScore.length; i++){
	   System.out.println(stuNo[i]+"\t"+stuName[i]+"\t"+stuScore[i]);
	   }
	   for (int i = 0; i < stuScore.length - 1; i++) {

			for (int j = 0; j < stuScore.length - i - 1; j++) {
				if (stuScore[j] > stuScore[j + 1]) {
					int num = stuScore[j];
					stuScore[j] = stuScore[j + 1];
					stuScore[j + 1] = num;
					
					String no = stuNo[j];
					stuNo[j] = stuNo[j + 1];
					stuNo[j + 1] = no;
					
					String name = stuName[j];
					stuName[j] = stuName[j + 1];
					stuName[j + 1] = name;
				}
			}
		}
	   
	   System.out.println("排序后");
	   for(int i = 0; i < stuScore.length; i++){
	   System.out.println(stuNo[i]+"\t"+stuName[i]+"\t"+stuScore[i]);
	   }

  }
   
  
    
public static void main(String [] args){
 
	   ArraysStudy.test1(stuScore);
	   ArraysStudy.test2(stuNo, stuName, stuScore);
	   ArraysStudy.test3(stuNo, stuName, stuScore);
	   ArraysStudy.test4(stuNo, stuName, stuScore);
    }
}
