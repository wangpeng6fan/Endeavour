package javahomework;

import java.util.Arrays;

public class sort {

	public static void main(String[] args) {
        String[] stuNo = {"2019011535","2019011534","2019011539","2019011538","2019011537"};    //学号
        String[] stuName = {"张三","李四","王五","赵六","王九"};  //姓名
        int[] stuScore = {53,78,96,66,85};      //成绩
        int sum = f(stuScore);      
        System.out.println("成绩排序前：");
        for (int i: stuScore) {
            System.out.print(i+" ");
        }
        System.out.println();
        Arrays.sort(stuScore);
        System.out.println("成绩排序后：");
        for (int i: stuScore) {
            System.out.print(i+" ");
        }
        System.out.println();
        System.out.println("最小值："+stuScore[0]);
        System.out.println("最大值："+stuScore[stuScore.length-1]);
        System.out.println("所有值之和："+sum);
        System.out.println("学号排序前：");
        for (String i: stuNo) {
            System.out.print(i+" ");
        }
        System.out.println();
        Arrays.sort(stuNo);
        System.out.println("学号排序后：");
        for (String i: stuNo) {
            System.out.print(i+" ");
        }
        System.out.println();

        System.out.println("姓名排序前：");
        for (String i: stuName) {
            System.out.print(i+" ");
        }
        System.out.println();
        Arrays.sort(stuName);
        System.out.println("姓名排序后：");
        for (String i: stuName) {
            System.out.print(i+" ");
        }
        System.out.println();
        // 第三题：
        String[] stuN = Arrays.copyOf(stuNo,stuNo.length);
        String[] stuNa = Arrays.copyOf(stuName,stuName.length);
        int[] stuSc = Arrays.copyOf(stuScore,stuScore.length);

        //第四题
        System.out.println("成绩   学号     姓名");
        for (int i = 0; i < stuScore.length ; i++) {
            System.out.print(" "+stuSc[i]+" ");
            System.out.print(stuN[i]+" ");
            System.out.print(stuNa[i]+" ");
            System.out.println();
        }

    }
    public static int f(int[] arr){
        int sum=0;
        for(int i=0;i < arr.length; i++){
            sum += arr[i];
        }
        return sum;
    }
}

