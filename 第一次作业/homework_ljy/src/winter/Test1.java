/**
 * 
 */
package winter;

/**
 *<p>Title:Test1</p>
 *<p>Description:</p>
 * @author ljy 
 * @data 2020年1月16日
 */
public class Test1 {

	/**
	 * @param args
	 */
	public static int getMax(int[] arr){
		int max = arr[0];
		for(int x=1; x<arr.length; x++){
		     if(arr[x]>max)
			max = arr[x];
		}
		return max;
	}
	public static int getMin(int[] arr){
		int min = arr[0];
		for(int x=1; x<arr.length; x++){
		     if(arr[x]<min)
			min = arr[x];
		}
		return min;
	}
	public static int getSum(int[] arr){
		int sum=0;
		for(int x=0; x<arr.length; x++){ 
			sum+=arr[x];
		}
		return sum;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] stuScore={53,78,96,66,85};
		int max = getMax(stuScore);
		int min = getMin(stuScore);
		int sum = getSum(stuScore);
		System.out.println("max="+max);
		System.out.println("min="+min);
		System.out.println("sum="+sum);
		int[] arr ={max,min,sum};
		System.out.println("源数组中的内容如下：");
	    //遍历源数组
	    for(int i=0;i<arr.length;i++)
	    {
	    System.out.print(arr[i]+"\t");
	    } 
	    //复制数组，将Object类型强制转换为int[]类型
	    int newArr[]=(int[])arr.clone();
	    System.out.println("\n目标数组内容如下："); 
	    //循环遍历目标数组
	    for(int k=0;k<newArr.length;k++)
	    {
	    System.out.print(newArr[k]+"\t");
	    }
		System.out.println("排序前");
		printArray(arr);
		System.out.println("排序后");
		pSort(arr);
		printArray(arr);
		
	}

	public static void printArray(int[] arr) {
		System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			if (i == arr.length - 1) {
				System.out.print(arr[i]);
			} else {
				System.out.print(arr[i] + ", ");
			}
		}
		System.out.println("]");
	}
	public static void pSort(int[] arr){
		for(int x=0;x<arr.length-1;x++){
			for(int y=x+1;y<arr.length;y++){
				if(arr[x]>arr[y])
				{
					int temp = arr[x];
					arr[x] = arr[y];
					arr[y] = temp;
				}
			}
		}
	}
}
