package com.ln.multiplication;



/**
  * 
 　 * <p>Title: Multiplication</p>
 　 * <p>Description: 打印10到15的乘法表</p>
 　 * @author ln
 　 * @date 2020年1月17日
  */
public class Multiplication {
        //当i=15时, m=0,不输出空格  j=10， 10*15=150  j=11  11*15....
	    //当i=14时，m=1，输出1个空格
	    //.....
	public static void main(String[] args) {
		for (int i = 15; i>=10; i--) {    //外层循环控制行数
			for(int m=0;m<15-i;m++){
				System.out.print("                ");      //输出每行的空格数
			}
			 for (int j =10; j<=i; j++) {   //内循环控制列数
				System.out.print(j+"*"+i+"="+i*j+"\t");
	     }
			 System.out.println();  //换行	
	  }
		
		
   }
 }
