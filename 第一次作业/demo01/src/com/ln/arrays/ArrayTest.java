package com.ln.arrays;

import java.util.Arrays;

public class ArrayTest {
	
	public static void paixun(){
		String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
		String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
		int[] stuScore = { 53, 78, 96, 66, 85};
		System.out.println("排序前的序列"+Arrays.toString(stuNo));
		System.out.println("排序前的序列"+Arrays.toString(stuName));
		System.out.println("排序前的序列"+Arrays.toString(stuScore));
		Arrays.sort(stuNo);
		Arrays.sort(stuName);
		Arrays.sort(stuScore);
		System.out.println("排序后的序列"+Arrays.toString(stuNo));
		System.out.println("排序后的序列"+Arrays.toString(stuName));
		System.out.println("排序后的序列"+Arrays.toString(stuScore));
		System.out.println("----------------------------------------------------------------------------------------------");
		
	}
	
	public static void copys(){
		String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
		String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
		int[] stuScore = { 53, 78, 96, 66, 85};		
		Arrays.sort(stuNo);
		Arrays.sort(stuName);
		Arrays.sort(stuScore);
		String[] stuNo1 = Arrays.copyOf(stuNo, stuNo.length);
		String[] stuName1 = Arrays.copyOf(stuName, stuName.length);
		int[] stuScore1 = Arrays.copyOf(stuScore, stuScore.length);
		System.out.println("复制后的数组stuNo1"+Arrays.toString(stuNo1));
		System.out.println("复制后的数组stuName1"+Arrays.toString(stuName1));
		System.out.println("复制后的数组stuScore1"+Arrays.toString(stuScore1));
		
	}
	
	
	public static void main(String[] args) {
		
		String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
		String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
		int[] stuScore = { 53, 78, 96, 66, 85 };
		int sum=0;
		Arrays.sort(stuScore);
		
		int max=stuScore[stuScore.length-1];
	    int min=stuScore[0];			
	    System.out.println("最大值为"+max);
		System.out.println("最小值为"+min);
		
		for(int i=0;i<stuScore.length;i++){
			sum+=stuScore[i];
		}
			System.out.println("所有值的和为"+sum);	
			System.out.println("-----------------------------------------------------------------------------------------");
			
			
			paixun();
			copys();
									
		}
	}
		

