package com.hly;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class HomeWork {
public static void main(String[] args) {
	work1();
	System.out.println("99乘法表");
	work2();//99乘法表
}

private static void work2() {
	for(int i=9;i>=1;i--){
			for( int j=1;j<=i;j++){
				System.out.print(j+"*"+i+"="+i*j+"\t");
			}
			System.out.println();
			for( int j=0;j<=9-i;j++){
			System.out.print("\t");
			}
		}
		System.out.println();
}

private static void work1() {
	String[] stuNo=new String[]{"2019011535","2019011534","2019011539","2019011538","2019011537"};
	String[] stuName=new String[]{"张三","李四","王五","赵六","王九"};
	int[] stuScore=new int[]{53,66,78,85,96};
	int max=stuScore[0];
	int min=stuScore[0];
	int sum=0;
	for(int i=0;i<stuScore.length;i++){
		//最大值
		if(stuScore[i]>max){
			max=stuScore[i];
		}
		//最小值
		if(stuScore[i]<min){
			min=stuScore[i];
		}
		sum+=stuScore[i];//所有值之和	
	}
	System.out.println("最大值:"+max);
	System.out.println("最小值:"+min);
	System.out.println("所有值之和:"+sum);
	//默认升序
	Arrays.sort(stuNo);
	Arrays.sort(stuName);
	Arrays.sort(stuScore);
	System.out.println("学号排序前:");
		for(int i=0;i<stuNo.length;i++){
			System.out.println(stuNo[i]);
		}
	System.out.println("学号排序后:");
	for(int i=stuNo.length-1;i>=0;i--){
		System.out.println(stuNo[i]);
	}
	System.out.println("名字排序前:");
	for(int i=stuName.length-1;i>=0;i--){
		System.out.println(stuName[i]);
	}
	System.out.println("名字排序后:");
	for(int i=0;i<stuName.length;i++){
		System.out.println(stuName[i]);
	}
	System.out.println("成绩排序前:");
	for(int i=0;i<stuScore.length;i++){
		System.out.println(stuScore[i]);
	}
	System.out.println("成绩排序后:");
	for(int i=stuScore.length-1;i>=0;i--){
		System.out.println(stuScore[i]);
		
	}
	//学号排序前+排序后
	String[] stuNoCopy=Arrays.copyOf(stuNo, 5);
	System.out.println("学号排序前+排序后:");
	String[] NewStuNo=new String[stuNo.length+stuNoCopy.length];
	System.arraycopy(stuNo, 0, NewStuNo, 0, stuNo.length);
	System.arraycopy(stuNoCopy, 0, NewStuNo, stuNo.length, stuNoCopy.length);
	String nsn=Arrays.toString(NewStuNo);
	System.out.println(nsn);
	//名字排序前+排序后
	String[] stuNameCopy=Arrays.copyOf(stuName, 5);
	System.out.println("名字排序前+排序后:");
	String[] NewStuName=new String[stuName.length+stuNameCopy.length];
	System.arraycopy(stuName, 0, NewStuName, 0, stuName.length);
	System.arraycopy(stuNameCopy, 0, NewStuName, stuName.length, stuNameCopy.length);
	String nsna=Arrays.toString(NewStuName);
	System.out.println(nsna);
	//成绩排序前+排序后
	int[] stuScoreCopy=Arrays.copyOf(stuScore, 5);
	System.out.println("成绩排序前+排序后:");
	int[] NewStuScore=new int[stuScore.length+stuScoreCopy.length];
	System.arraycopy(stuScore, 0, NewStuScore, 0, stuScore.length);
	System.arraycopy(stuScoreCopy, 0, NewStuScore, stuScore.length, stuScoreCopy.length);
	String nsc=Arrays.toString(NewStuScore);
	System.out.println(nsc);
}

}
