package firstHomeWork;

import java.util.Arrays;
import java.util.stream.IntStream;


public class ArraysTest {
	
	
	
	public static void main(String [] args){
		 
		 ArraysTest.test1(stuScore);
	//	 ArraysTest.test2(stuNo, stuName, stuScore);
	//	 ArraysTest.test3(stuNo, stuName, stuScore);
	//	 ArraysTest.test4(stuNo, stuName, stuScore);
	 }

	
	
	
	static String[] stuNo = { "2019011535", "2019011534", "2019011539", "2019011538", "2019011537" };
	
	static 	String[] stuName = { "张三", "李四", "王五", "赵六", "王九" };
	
    static int[] stuScore = { 53, 78, 96, 66, 85 };
 
    //1.计算成绩数组中最大值、最小值、所有值之和，并输出到控制台
    public static void test1(int[] stuScore){	
    	IntStream intStream =Arrays.stream(stuScore);
    	Arrays.sort(stuScore);
    	int Min = stuScore[0];
    	int Max = stuScore[stuScore.length - 1];
        int sum = intStream.sum();
        
       
		System.out.println("最小值："+Min);
		System.out.println("最大值："+Max);
		System.out.println("总和："+sum);
    }
    
    //2.分别对以上三组数据排序，向控制台输出排序前和排序后的内容
    public static void test2( String[] stuNo,String[] stuName,int[] stuScore){
	  	//学号
	  	System.out.println("排序前的学号数组："+Arrays.toString(stuNo));
	  	Arrays.sort(stuNo);
	  	System.out.println("排序后的学号数组："+Arrays.toString(stuNo));
	
	  	//姓名
	  	System.out.println("排序前的姓名数组："+Arrays.toString(stuName));
	  	Arrays.sort(stuName);
	  	System.out.println("排序后的姓名数组："+Arrays.toString(stuName));
	
	  	//成绩
		System.out.println("排序前的成绩数组："+Arrays.toString(stuScore));
		Arrays.sort(stuScore);
		System.out.println("排序后的成绩数组："+Arrays.toString(stuScore));
		}
  
  //3.将（2）中排序好的内容复制到另外三个数组中
    public static void test3(String[] stuNo, String[] stuName, int[] stuScore) {
		int[] newStuScore = Arrays.copyOf(stuScore, 5);
		System.out.println(Arrays.toString(newStuScore));

		String[] newStuNo = Arrays.copyOf(stuNo, stuNo.length);
		System.out.println(Arrays.toString(newStuNo));

		String[] newStuName = Arrays.copyOf(stuName, stuName.length);
		System.out.println(Arrays.toString(newStuName));
	}
  
  //4.根据成绩排序，要求保持学号、姓名、成绩对应（选作）
    public static void test4(String[] stuNo,String[] stuName,int[] stuScore){
	    System.out.println("排序前:");
	    for(int i = 0; i < stuScore.length; i++){
	    System.out.println(stuNo[i]+"\t"+stuName[i]+"\t"+stuScore[i]);
	    }
	    for (int i = 0; i < stuScore.length - 1; i++) {

			 for (int j = 0; j < stuScore.length - i - 1; j++) {
		 		 if (stuScore[j] > stuScore[j + 1]) {
				 	 int num = stuScore[j];
					 stuScore[j] = stuScore[j + 1];
					 stuScore[j + 1] = num;
					
					 String no = stuNo[j];
					 stuNo[j] = stuNo[j + 1];
					 stuNo[j + 1] = no;
					
					 String name = stuName[j];
					 stuName[j] = stuName[j + 1];
					 stuName[j + 1] = name;
				}
			}
		}
	   
	   System.out.println("排序后:");
	   for(int i = 0; i < stuScore.length; i++){
	   System.out.println(stuNo[i]+"\t"+stuName[i]+"\t"+stuScore[i]);
	   }
  }
}
